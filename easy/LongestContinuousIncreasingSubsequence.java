class LongestContinuousIncreasingSubsequence {
    public int findLengthOfLCIS(int[] nums) {
        int maxRun = 1;
        int run = 1;
        
        for (int x = 1; x < nums.length; x++) {
            if (nums[x] > nums[x -1]) {
                run += 1;
                if (run > maxRun) {
                    maxRun = run;
                }
            } else {
                run = 1;
            }
        }
        
        return maxRun;
    }
}
