class KthLargestElementInStream {
    protected int[] values;

    public KthLargest(int k, int[] nums) {
        this.values = new int[k];
        for (int i=0; i<k; i++) {
            this.values[i] = Integer.MIN_VALUE;
        }
        
        for (int num : nums) {
            this.add(num);
        }
    }
    
    public int add(int val) {
        if (this.values.length == 1) {
            this.values[0] = Math.max(this.values[0], val);
        }
        
        if (val > this.values[0]) {
            for (int i = 1; i < this.values.length; i++) {
                if (val >= this.values[i]) {
                    this.values[i - 1] = this.values[i];
                } else {
                    this.values[i - 1] = val;
                    break;
                }
                
                if (i == this.values.length - 1) {
                    this.values[i] = val;
                }
            }
        }
        
        return this.values[0];
    }
}

/**
 * Your KthLargest object will be instantiated and called as such:
 * KthLargest obj = new KthLargest(k, nums);
 * int param_1 = obj.add(val);
 */
