class ValidParenthesis {
    public boolean isValid(String s) {
        // not valid if an uneven number of characters
        if (s.length() % 2 == 1) {
            return false;
        }
        
        LinkedList<Character> opens = new LinkedList<>();
        for (char currentCharacter : s.toCharArray()) {
            if (currentCharacter == '(' || currentCharacter == '{' || currentCharacter == '[') {
                opens.push(currentCharacter);
            } else {
                char poppedCharacter = opens.pop();
                
                if (currentCharacter == ')' && poppedCharacter != '(') {
                    return false;
                } else if (currentCharacter == '}' && poppedCharacter != '{') {
                    return false;
                } else if (currentCharacter == ']' && poppedCharacter != '[') {
                    return false;
                }
            }
        }
        
        // anything left on the stack means it was invalid
        if (opens.size() > 0) {
            return false;
        }
        
        return true;
    }
}
