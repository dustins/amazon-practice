class CountPrimes {
    public boolean isPrime(int n) {      
        if (n == 2 || n == 3 || n == 5 || n == 7) {
            return true;
        }
        
        if (n == 1 || n % 2 == 0 || n % 5 == 0 || n % 3 == 0 || n % 7 == 0) {
            return false;
        }
        
        return true;
    }
    
    public int countPrimes(int n) {
        int count = 0;
        
        for (int x = 1; x < n; x++) {
            count += isPrime(x) ? 1 : 0;    
        }
        
        return count;
    }
}
