class NonDecreasingArray {
    public boolean checkPossibility(int[] nums) {
        if (nums.length <= 1) {
            return true;
        }
        
        int changes = 0;
        for (int x = 1; x < nums.length; x++) {
            if (nums[x - 1] > nums[x]) {
                changes += 1;
                
                if (x == 1 || x == nums.length - 1) {
                    continue;
                }
                
                if (nums[x - 1] <= nums[x + 1]) {
                    continue;
                }
                
                if (nums[x - 2] <= nums[x]) {
                    continue;
                }
                
                return false;
            }
        }
        
        return changes < 2;
    }
}
