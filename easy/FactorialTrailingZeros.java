class FactorialTrailingZeros {   
    public int trailingZeroes(int n) {
        int count = 0;
        for (int x = 5; n / x >= 1; x *= 5) {
            count += n / x;
        }
        return count;
    }
}
