class DesignHashSet {
    protected ArrayList<Integer> values;

    public MyHashSet() {
        this.values = new ArrayList<>();
    }
    
    public void add(int key) {
        if (!this.values.contains(key)) {
            this.values.add(key);
        }
    }
    
    public void remove(int key) {
        if (this.values.contains(key)) {
            int index = this.values.indexOf(key);
            this.values.remove(index);
        }
    }
    
    public boolean contains(int key) {
        return this.values.contains(key);    
    }
}

/**
 * Your MyHashSet object will be instantiated and called as such:
 * MyHashSet obj = new MyHashSet();
 * obj.add(key);
 * obj.remove(key);
 * boolean param_3 = obj.contains(key);
 */
