/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class SubTreeOfAnotherTree {
    protected boolean subTreeMatch(TreeNode root, TreeNode subRoot) {
        if (root == null && subRoot == null) {
            return true;
        } else if (root == null || subRoot == null) {
            return false;
        }
        
        return (root.val == subRoot.val &&
            subTreeMatch(root.left, subRoot.left) &&
            subTreeMatch(root.right, subRoot.right));
    }
    
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {   
        if (root == null) {
            return false;
        }
        
        if (subRoot == null) {
            return true;
        }
        
        return subTreeMatch(root, subRoot) ||
            isSubtree(root.left, subRoot) ||
            isSubtree(root.right, subRoot);
    }
}
