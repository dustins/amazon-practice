class PeakIndexInMountainArray {
    public int peakIndexInMountainArray(int[] arr) {       
        for (int x = 0; x < arr.length - 1; x++) {
            if (arr[x] > arr[x + 1]) {
                return x;
            }
        }
        
        return 0;
    }
}
