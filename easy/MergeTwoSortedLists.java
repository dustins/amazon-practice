/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class MergeTwoSortedLists {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null && list2 == null) {
            return null;
        }
        
        ListNode result, tail;
        if (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                result = new ListNode(list1.val);
                list1 = list1.next;
            } else {
                result = new ListNode(list2.val);
                list2 = list2.next;
            }
        } else {
            // default list1 to the non-null list
            list1 = list1 != null ? list1 : list2;
            list2 = null;
            result = new ListNode(list1.val);
            list1 = list1.next;
        }
        
        tail = result;
        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                tail.next = new ListNode(list1.val);
                tail = tail.next;
                list1 = list1.next;
            } else {
                tail.next = new ListNode(list2.val);
                tail = tail.next;
                list2 = list2.next;
            }
        }
        
        // add any remaining nodes
        list1 = list1 != null ? list1 : list2;
        while (list1 != null) {
            tail.next = new ListNode(list1.val);
            tail = tail.next;
            list1 = list1.next;
        }
        
        return result;
    }
}
