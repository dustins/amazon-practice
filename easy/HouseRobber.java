class HouseRobber {
    public int rob(int[] nums) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            return nums[0];
        } else if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        
        int[] solution = new int[nums.length];
        solution[0] = nums[0];
        solution[1] = Math.max(nums[0], nums[1]);
        for (int x=2; x<nums.length; x++) {
            solution[x] = Math.max(nums[x]+solution[x-2], solution[x-1]);
        }
        
        return solution[nums.length - 1];
    }
}
