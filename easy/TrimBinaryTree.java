/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class TrimBinaryTree {
    public TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) {
            return null;
        }
        
        if (root.val < low || root.val > high) {
            TreeNode newNode;
            if (root.left != null) {
                newNode = trimBST(root.left, low, high);
                if (newNode != null) {
                    return newNode;
                }
            }
            
            if (root.right != null) {
                newNode = trimBST(root.right, low, high);
                if (newNode != null) {
                    return newNode;
                }
            }
            
            return null;
        }
        
        root.left = trimBST(root.left, low, high);
        root.right = trimBST(root.right, low, high);
        
        return root;
    }
}
