class FinalAllNumbersDisappearedInArray {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> solution = new ArrayList<>();
        
        for (int x = 0; x < nums.length; x++) {
            int index = Math.abs(nums[x]) - 1;
            if (nums[index] > 0) {
                nums[index] *= -1;
            }
        }
        
        for (int x = 0; x < nums.length; x++) {
            if (nums[x] > 0) {
                solution.add(x + 1);
            }
        }
        
        return solution;
    }
}
