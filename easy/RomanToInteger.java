class RomanToInteger {
    public int romanToInt(String s) {
        int accumulator = 0;
        int length = s.length() - 1;
        
        Map<Character, Integer> symbolMap = new HashMap<>();
        symbolMap.put('I', 1);
        symbolMap.put('V', 5);
        symbolMap.put('X', 10);
        symbolMap.put('L', 50);
        symbolMap.put('C', 100);
        symbolMap.put('D', 500);
        symbolMap.put('M', 1000);
        
        for (int currentIndex = 0; currentIndex <= length; currentIndex++) {
            char currentCharacter = s.charAt(currentIndex);
            int modifier = symbolMap.get(currentCharacter);
            
            if (currentIndex < length) {
                Character peekCharacter = s.charAt(currentIndex+1);
                switch (currentCharacter) {
                    case 'I':
                        if (peekCharacter.equals('V') || peekCharacter.equals('X')) {
                            modifier *= -1;
                        }
                        break;
                    case 'X':
                        if (peekCharacter.equals('L') || peekCharacter.equals('C')) {
                            modifier *= -1;
                        }
                        break;
                    case 'C':
                        if (peekCharacter.equals('D') || peekCharacter.equals('M')) {
                            modifier *= -1;
                        }
                        break;
                }
            }
            
            accumulator += modifier;
        }
        
        return accumulator;
    }
}
