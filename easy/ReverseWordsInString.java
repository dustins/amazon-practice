import java.util.stream.Collectors;

class ReverseWordsInString {
    public String reverseWords(String s) {
        return Arrays.stream(s.split("\\s"))
            .map((word) -> {
                StringBuilder newWord = new StringBuilder();
                newWord.append(word);
                newWord.reverse();
                return newWord.toString();
            })
            .collect(Collectors.joining(" "));
    }
}
