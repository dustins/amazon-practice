class RotateArray {
    public void rotate(int[] nums, int k) {
        int[] tempSolution = new int[nums.length];
        for (int x = 0; x < nums.length; x++) {
            tempSolution[(x + k) % nums.length] = nums[x];
        }
        
        for (int x = 0; x < nums.length; x++) {
            nums[x] = tempSolution[x];
        }
    }
}
