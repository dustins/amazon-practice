import java.util.Optional;

class MinStack {
    private Optional<Element> top;
    
    class Element {
        int value;
        int min;
        Optional<Element> next;
        
        public Element(int value, int min, Optional<Element> next) {
            this.value = value;
            this.min = min;
            this.next = next;
        }
    }

    public MinStack() {
        this.top = Optional.empty();
    }
    
    public void push(int val) {
        Element newTop;
        
        if (this.top.isPresent()) {
            newTop = new Element(val, this.top.get().min < val ? this.top.get().min : val, this.top);
        } else {
            newTop = new Element(val, val, null);
        }
        
        this.top = Optional.of(newTop);
    }
    
    public void pop() {
        this.top = this.top.get().next;
    }
    
    public int top() {
        return this.top.get().value;
    }
    
    public int getMin() {
        return this.top.get().min;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
