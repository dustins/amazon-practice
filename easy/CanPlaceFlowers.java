class CanPlaceFlowers {
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        if (n == 0) {
            return true;
        }
        
        if (n > flowerbed.length) {
            return false;
        }
        
        for (int x = 0; x < flowerbed.length; x++) {
            boolean canPlace = flowerbed[x] == 0;
            
            if (canPlace && x > 0) {
                canPlace = canPlace && flowerbed[x - 1] == 0;
            }
            
            if (canPlace && x < flowerbed.length - 1) {
                canPlace = canPlace && flowerbed[x + 1] == 0;
            }
            
            if (canPlace) {
                n -= 1;
                flowerbed[x] = 1;
            }
        }
        
        return n < 1;
    }
}
