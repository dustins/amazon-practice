class HappyNumber {   
    public int sumOfSquares(int n) {
        int sum = (n % 10) * (n % 10);
        
        while (n > 0) {
            n = n / 10;
            sum += (n % 10) * (n % 10);
        }
        
        return sum;
    }
    
    public boolean isHappy(int n) {
        Set<Integer> loopDetector = new HashSet<>();
        
        do {
            n = sumOfSquares(n);
            if (loopDetector.contains(n)) {
                return false;
            }
            
            loopDetector.add(n);
        } while (n != 1);
            
        return true;
    }
}
