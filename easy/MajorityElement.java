class MajorityElement {
    public int majorityElement(int[] nums) {
        int accumulator = 0;
        int majorityCandidate = 0;
        
        for (int x = 0; x < nums.length; x++) {
            if (accumulator == 0) {
                majorityCandidate = nums[x];
            }
            
            if (nums[x] == majorityCandidate) {
                accumulator += 1;
            } else {
                accumulator -= 1;
            }
        }
        
        return majorityCandidate;
    }
}
