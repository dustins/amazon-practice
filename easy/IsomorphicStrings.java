class IsomorphicStrings {
    public boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        
        Map<Character, Character> replacements = new HashMap<>();
        for (int x = 0; x < s.length(); x++) {
            if (replacements.containsKey(s.charAt(x))) {
                if (replacements.get(s.charAt(x)) != t.charAt(x)) {
                    return false;
                }
            } else if (!replacements.containsValue(t.charAt(x))) {
                replacements.put(s.charAt(x), t.charAt(x));
            } else {
                return false;
            }
        }
        
        return true;
    }
}
