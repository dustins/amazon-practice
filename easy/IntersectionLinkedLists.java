/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class IntersectionLinkedLists {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Set<ListNode> previousNodes = new HashSet<>();
        while (headA != null) {
            previousNodes.add(headA);
            headA = headA.next;
        }
        
        while (headB != null) {
            if (previousNodes.contains(headB)) {
                return headB;
            }
            
            headB = headB.next;
        }
        
        return null;
    }
}
