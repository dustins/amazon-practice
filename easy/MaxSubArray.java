class MaxSubArray {
    public int maxSubArrayCrossing(int[] nums, int start, int end) {
        int sum = 0;
        int midpoint = (start + end) / 2;

        sum = 0;
        int leftSum = Integer.MIN_VALUE;
        for (int x = midpoint; x >= start; x--) {
            sum += nums[x];
            if (sum > leftSum) {
                leftSum = sum;
            }
        }
        
        sum = 0;
        int rightSum = Integer.MIN_VALUE;
        for (int x = midpoint + 1; x <= end; x++) {
            sum += nums[x];
            if (sum > rightSum) {
                rightSum = sum;
            }
        }
            
        return Math.max(
            leftSum + rightSum,
            Math.max(leftSum, rightSum)
        );
    }
    public int maxSubArraySum(int[] nums, int start, int end) {
        if (start == end) {
            return nums[start];
        }
        
        int midpoint = (start + end) / 2;
        
        return Math.max(
            Math.max(
                maxSubArraySum(nums, start, midpoint),
                maxSubArraySum(nums, midpoint + 1, end)
            ),
            maxSubArrayCrossing(nums, start, end)
        );
    }
    
    public int maxSubArray(int[] nums) {
        return maxSubArraySum(nums, 0, nums.length - 1);
    }
}
